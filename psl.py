# https://bit.ly/2TgFziL

# tomasz.j.wesolowski@gmail.com

import os

def ret_time(line):

	line = line.strip(',').strip('s')
	if ':' in line:
		time = line.split(':')
		return  int(time[0]), int(time[1]) 
	else:
		return  int(line)//60, int(line)%60 

    # return number of unique words	
def count_words(data): #-> int 
    nazwa = set()
    for line in data:
        for word in line.split(' '):
            nazwa.add(word.strip(',').strip('.').strip('!').strip('?').strip(':').lower())
    return len(nazwa)

for (dirpath, dirname, filename) in os.walk('./songs'):

    for one_file in filename:
        f = open(dirpath+'/'+one_file)
        data = f.readlines()
        print(data[0].strip('\n') + " " +data[2].strip('\n').strip(','))
        min_, sec = ret_time(data[2].strip('\n').strip()) 
        count_ = count_words( data[3:])      
        print( "{} min {} sec {} words".format(min_, sec, count_))


        print('---------------')
        f.close()

        # sl['the beatles']['hey jude'] => {'time': 191, 'wc': 88}
from pprint import pprint as dupa
d = {1 :  {'a', 'b'},2:  {'e', 'd'}}
dupa(d)